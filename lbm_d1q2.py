import numpy as np
import matplotlib.pyplot as plt

class LBM_D1Q2:
    def __init__(self, f_0_0, f_1_0, s_1, m_1_eq, dx=0.01, x_0=0., x_1=1., dt=0.01, t_0=0., t_1=2.):
        """
        Initializes the LBM_D1Q2 class
        
        Args:
            - dx (float): space step
            - x_0 (float): left boundary
            - x_1 (float): right boundary
            - dt (float): time step
            - t_0 (float): initial time
            - t_1 (float): final time
            - f_0_0 (function: x -> f_0_0(x)): initial value of f_0
            - f_1_0 (function: x -> f_1_0(x)): inintial value of f_1
            - s_1 (float): relaxation parameter
            - m_1_eq (function: m_0 -> m_1_eq(m_0)): equilibrium distribution of m_1 in term of m_0 = f_0 + f_1
        """
        self.dx = dx
        self.nx = int((x_1 - x_0) / dx + 1)
        self.dt = dt
        self.nt = int((t_1 - t_0) / dt + 1)

        self.X = np.linspace(x_0, x_1, self.nx)
        self.T = np.linspace(t_0, t_1, self.nt)

        self.m_1_eq = m_1_eq
        self.s_1 = s_1
        self.relax_mat = np.array([[0., 0.], [0., s_1]])

        self.lamb = dx / dt
        self.M = np.array([[1, 1], [self.lamb, -self.lamb]])
        self.M_inv = np.array([[0.5, 0.5 / self.lamb], [0.5, -0.5 / self.lamb]])

        self.f = np.zeros((self.nx, self.nt, 2))
        self.f_star = np.zeros((self.nx, self.nt, 2))
        self.f[:,0,0], self.f[:,0,1] = f_0_0(self.X), f_1_0(self.X)

        self.m = np.zeros((self.nx, self.nt, 2))

        self.m_eq = np.zeros((self.nx, self.nt, 2))

    def relaxation(self, it):
        """
        Relaxation step of the LBM
        
        Args:
            - it (int): time index
        """
        for ix in range(self.nx):
            self.m[ix,it,:] = np.matmul(self.M, self.f[ix,it,:])

            self.m_eq[ix,it,0] = self.m[ix,it,0]
            self.m_eq[ix,it,1] = self.m_1_eq(self.m_eq[ix,it,0])

            self.f_star[ix,it,:] = np.matmul(self.M_inv, self.m[ix,it,:] + np.matmul(self.relax_mat, self.m_eq[ix,it,:] - self.m[ix,it,:]))
    
    def transport(self, it):
        """
        Transport step of the LBM from it to it+1
        
        Args:
            - it (int): time index
        """
        self.f[:,it+1,0] = np.roll(self.f_star[:,it,0], 1)
        self.f[:,it+1,1] = np.roll(self.f_star[:,it,1], -1)

    def plot_m_0(self, it):
        """
        Plot the m_0 distribution at time it
        
        Args:
            - it (int): time index
        """
        plt.plot(self.X, self.m[:,it,0])
        plt.show()

    def run(self):
        """
        Run the LBM
        """
        for it in range(self.nt-1):
            self.relaxation(it)
            self.transport(it)
        # last update of m
        self.m[:,self.nt-1,:] = np.array([np.matmul(self.M, self.f[ix,self.nt-1,:]) for ix in range(self.nx)])




# Test the LBM

def hat(x):
    if x < 0.25 or x > 0.75:
        return 0.
    elif x < 0.5:
        return 4*(x-0.25)
    else:
        return 4*(0.75-x)
vec_hat = np.vectorize(hat)

# Test for advection equation
c = 0.5
f_0_0, f_1_0, s_1, m_1_eq = vec_hat, vec_hat, 1.8, lambda m_0: c*m_0
test1 = LBM_D1Q2(f_0_0, f_1_0, s_1, m_1_eq)
test1.run()
test1.plot_m_0(0)
test1.plot_m_0(5)
test1.plot_m_0(20)
test1.plot_m_0(100)
test1.plot_m_0(200)

f_0_0, f_1_0, s_1, m_1_eq = lambda x: 0.125*vec_hat(x), lambda x: 0.125*vec_hat(x), 1.8, lambda m_0: 0.5*m_0*m_0
test2 = LBM_D1Q2(f_0_0, f_1_0, s_1, m_1_eq, t_1=8.)
test2.run()
test2.plot_m_0(0)
test2.plot_m_0(5)
test2.plot_m_0(20)
test2.plot_m_0(100)
test2.plot_m_0(200)
test2.plot_m_0(400)
test2.plot_m_0(800)
